<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Company Info | Property List</title>

        <link rel="stylesheet" href="/css/app.css">
    </head>
    <body>
    <div class="content">
        <h1 class="title">Property List for {{ $company->name }}</h1>
        <p><a href="/">Return to Company List</a></p>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Property Name</th>
                    <th scope="col">Contact</th>
                    <th scope="col">Address</th>
                    {{--<th scope="col">Phone</th>--}}
                </tr>
                </thead>
                <tbody>
                @foreach ($properties as $property)
                    <tr>
                        <td>{{ $property->name }}</td>
                        <td>{{ $property->contact }}</td>
                        <td>{{ $property->address }} <br> {{ $property->city }}, {{ $property->state }} {{ $property->zip }}</td>
                        {{--<td>{{ $property->phone }}</td>--}}
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    </body>
</html>

