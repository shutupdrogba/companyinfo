## Simple 2 page site that displays companies and their associated properties.

##Built with Laravel Homestead
1) Used the Laravel template found here: https://github.com/laravel/laravel

##Database dumps are located in storage/databases
1. 1 dump files for 2 different tables (companies and properties)
2. Database config settings can be modified in */config/database.php* and */.env*

##To compile (if necessary)
1. Run npm install in your terminal
2. Run "npm run dev" or "npm run watch" for quick development
3. Run "npm run prod" for production version

##Viewing site
1. Compiled assets are in the */public* folder
2. Point server domain to the */public* folder

##Contact
rabih.sallman@gmail.com 
