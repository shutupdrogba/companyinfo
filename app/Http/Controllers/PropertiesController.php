<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class PropertiesController extends Controller
{
    /**
     * Show all properties for this company.
     *
     * @return Response
     */
    public function getProperty($id)
    {
        $company = DB::table('companies')->where('ID', $id)->first();
        $properties = DB::table('properties')->where('ID', $id)->get();

        return view('properties', ['company'=>$company, 'properties'=>$properties]);
    }
}