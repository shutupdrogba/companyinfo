<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class CompaniesController extends Controller
{
    /**
     * Show a list of all of the companies.
     *
     * @return Response
     */
    public function index()
    {
        $companies = DB::table('companies')->get();

        return view('companies', ['companies'=>$companies]);
    }
}