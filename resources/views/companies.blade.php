<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Company Info | Company List</title>

        <link rel="stylesheet" href="/css/app.css">
    </head>
    <body>
    <div class="content">
        <h1 class="title">Company List</h1>
        <p>Click the company name below to see a list of their properties.</p>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Company Name</th>
                    <th scope="col">Founded</th>
                    <th scope="col">Address</th>
                    <th scope="col">Phone</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($companies as $company)
                    <tr>
                        <td><a href="{{route('getProperty', $company->ID)}}">{{ $company->name }}</a></td>
                        <td>{{substr($company->founded, 0, 4)}}</td>
                        <td>{{ $company->address }} <br> {{ $company->city }}, {{ $company->state }} {{ $company->zip }}</td>
                        <td>{{ $company->phone }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    </body>
</html>

